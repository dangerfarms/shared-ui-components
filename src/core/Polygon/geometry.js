function toRadian(deg) {
  return (deg / 180) * Math.PI;
}

export function calculatePoints(n, size) {
  const x = size / 2;
  const y = 0;

  const r = size / 2;
  const centerAngle = 360 / n;
  const points = [];

  for (let i = 0; i < n; i++) {
    const innerAngle = centerAngle * i;
    const innerAngleRad = toRadian(innerAngle);
    const cosInnerAngleRad = Math.cos(innerAngleRad);

    const tangentAngleRad = innerAngleRad / 2;
    const sinTangentAngleRad = Math.sin(tangentAngleRad);
    const cosTangentAngleRad = Math.cos(tangentAngleRad);

    const contourSegment = Math.sqrt(2) * r * Math.sqrt(1 - cosInnerAngleRad);
    points.push([
      x + contourSegment * cosTangentAngleRad,
      y + contourSegment * sinTangentAngleRad,
    ]);
  }

  const FLOATING_POINT_PRECISION = 14;
  return points.map(point =>
    point.map(v => Number(v.toPrecision(FLOATING_POINT_PRECISION))),
  );
}
