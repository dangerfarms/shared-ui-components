### The Polygon component can be used with animejs to create a spinner as follows:

<br />

```react
class Spinner extends Component {
  constructor(props) {
    super(props);
    this.polygonRef = React.createRef();
  }

  componentDidMount() {
    anime({
      targets: this.polygonRef.current,
      duration: this.props.duration,
      easing: "linear",
      loop: true,
      rotate: "360deg",
    });
  }

  render() {
    return (
      <Polygon
        className={this.props.className}
        fill={this.props.fill}
        hostRef={this.polygonRef}
        sides={this.props.sides}
        size={this.props.size}
      />
    );
  }
}

Spinner.propTypes = {
  className: PropTypes.string,
  duration: PropTypes.number,
  fill: PropTypes.oneOfType([
    PropTypes.shape({
      angle: PropTypes.number,
      endColor: PropTypes.string.isRequired,
      startColor: PropTypes.string.isRequired,
    }),
    PropTypes.string,
  ]).isRequired,
  sides: PropTypes.number,
  size: PropTypes.number.isRequired,
};

Spinner.defaultProps = {
  className: null,
  duration: 2000,
  sides: 6,
};
```
