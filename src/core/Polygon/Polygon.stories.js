/* TODO: Investigate why this is required. My instinct is that the storysource addon is causing the transpiler
output to break this rule, as commenting out its configuration in webpack config fixes the issue. */
/* eslint-disable import/first */
import React from "react";
import { storiesOf } from "@storybook/react";
import { withKnobs, color, number } from "@storybook/addon-knobs";
import Polygon from "./Polygon";
import markdownNotes from "./polygonStoryNotes.md";

const polygonStory = storiesOf("Polygon", module);

polygonStory.addDecorator(withKnobs);

polygonStory.add(
  "a polygon!",
  () => {
    const colour = color("Colour", "#000");
    const size = number("Size", 100);

    return <Polygon fill={colour} size={size} />;
  },
  { notes: { markdown: markdownNotes } },
);
