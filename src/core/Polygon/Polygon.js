import cuid from "cuid";
import PropTypes from "prop-types";
import React, { Component } from "react";
import { calculatePoints } from "./geometry";
import { hostRef } from "./propTypes";
import LinearGradient from "./LinearGradient";

export default class Polygon extends Component {
  constructor(props) {
    super(props);

    this.state = {
      svgSize: null,
    };
  }

  componentDidMount() {
    // Update the value in state to force a re-render - explanation below
    this.setState({ svgSize: this.props.size + 1 });
  }

  componentDidUpdate(prevProps) {
    // We need to keep the size in state in sync with the size from props since we use the value from
    // state if it's present and we still need to react to the window being resized.

    // It's fine to set state here since we're doing it conditionally
    if (prevProps.size !== this.props.size) {
      /* eslint-disable react/no-did-update-set-state */
      this.setState({ svgSize: this.props.size });
      /* eslint-enable react/no-did-update-set-state */
    }
  }

  render() {
    // One of the all-time most shameful, dirty hacks of my career.
    //
    // The reason for this is because the svg height/width values do not update from the initial values generated
    // during SSR until the value *changes* during runtime. To hack around this we force a change, where mounting the
    // component will add 1 to the size. The subsequent render will update the DOM.
    //
    // Render #0 (at build time):   <svg height={null} width={null}>    =>    <svg height="0" width="0">
    // Render #1 (client-side):     <svg height={1001} width={1001}>    =>    <svg height="0" width="0">
    // Render #2 (client-side):     <svg height={1000} width={1000}>    =>    <svg height="1000" width="1000">
    //
    // I think this could potentially be a bug in React but not sure. The polygon points (for example) *do* get
    // updated with the size value calculated at runtime without any hack.
    //
    // Would be interesting to try and repro in codepen.

    // On render #1, we'll be using the value from props.
    // On render #2, following the state update in componentDidMount, we'll be using the value from state.
    const sizeToUse = this.state.svgSize || this.props.size;

    if (typeof this.props.fill === "string") {
      return (
        <svg
          ref={this.props.hostRef}
          className={this.props.className}
          height={sizeToUse}
          width={sizeToUse}
        >
          <polygon
            fill={this.props.fill}
            points={calculatePoints(this.props.sides, this.props.size)}
          />
        </svg>
      );
    }

    // Create a unique ID for this polygon's fill. A random one ensures multiple
    // polygons don't cross wires. It's OK to re-generate the ID on re-render:
    // although it'll update the DOM more often than needed, there doesn't seem
    // to be any perf/consistency issue
    const id = cuid();

    return (
      <svg
        ref={this.props.hostRef}
        className={this.props.className}
        height={sizeToUse}
        width={sizeToUse}
      >
        <defs>
          <LinearGradient
            angle={this.props.fill.angle}
            endColor={this.props.fill.endColor}
            id={id}
            startColor={this.props.fill.startColor}
          />
        </defs>
        <polygon
          fill={`url(#${id})`}
          points={calculatePoints(this.props.sides, this.props.size)}
        />
      </svg>
    );
  }
}

Polygon.propTypes = {
  className: PropTypes.string,
  fill: PropTypes.oneOfType([
    PropTypes.shape({
      angle: PropTypes.number,
      endColor: PropTypes.string.isRequired,
      startColor: PropTypes.string.isRequired,
    }),
    PropTypes.string,
  ]).isRequired,
  hostRef,
  sides: PropTypes.number,
  size: PropTypes.number.isRequired,
};

Polygon.defaultProps = {
  className: null,
  hostRef: null,
  sides: 6,
};
