import PropTypes from "prop-types";
import React from "react";

/**
 * A simple SVG linearGradient between two colors, optionally with a rotation.
 */
export default function LinearGradient(props) {
  return (
    <linearGradient
      gradientTransform={`rotate(${props.angle} 0.5 0.5)`}
      gradientUnits="objectBoundingBox"
      id={props.id}
    >
      <stop offset="0%" stopColor={props.startColor} />
      <stop offset="100%" stopColor={props.endColor} />
    </linearGradient>
  );
}

LinearGradient.propTypes = {
  angle: PropTypes.number,
  endColor: PropTypes.string.isRequired,
  id: PropTypes.string.isRequired,
  startColor: PropTypes.string.isRequired,
};

LinearGradient.defaultProps = {
  angle: 0,
};
