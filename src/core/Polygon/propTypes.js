import PropTypes from "prop-types";

export const hostRef = PropTypes.oneOfType([PropTypes.func, PropTypes.shape()]);
