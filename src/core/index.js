export { default as Modal } from "./Modal/Modal";
export { default as ModalButton } from "./Modal/ModalButton";
export { default as ModalSubmitButton } from "./Modal/ModalSubmitButton";
export { default as Polygon } from "./Polygon/Polygon";
export { default as Spinner } from "./Spinner/Spinner";
