import React, { Component } from "react";
import anime from "animejs";
import PropTypes from "prop-types";
import Polygon from "../Polygon/Polygon";

class Spinner extends Component {
  constructor(props) {
    super(props);
    this.polygonRef = React.createRef();
  }

  componentDidMount() {
    if (this.props.scale) {
      this.doScale();
    } else {
      anime({
        targets: this.polygonRef.current,
        duration: this.props.duration,
        easing: "linear",
        loop: true,
        rotate: "360deg",
      });
    }
  }

  componentDidUpdate() {
    if (this.props.scale) {
      anime.remove(this.polygonRef.current);
      this.doScale();
    }
  }

  doScale() {
    const scale = anime({
      targets: this.polygonRef.current,
      duration: this.props.scale.duration,
      easing: "linear",
      rotate: "360deg",
      scale: [this.props.scale.from, this.props.scale.to],
    });

    scale.finished.then(() => {
      anime.remove(this.polygonRef.current);
      if (this.props.scale.callback) {
        this.props.scale.callback();
      }
    });
  }

  render() {
    return (
      <Polygon
        className={this.props.className}
        fill={this.props.fill}
        hostRef={this.polygonRef}
        sides={this.props.sides}
        size={this.props.size}
      />
    );
  }
}

Spinner.propTypes = {
  className: PropTypes.string,
  duration: PropTypes.number,
  fill: PropTypes.oneOfType([
    PropTypes.shape({
      angle: PropTypes.number,
      endColor: PropTypes.string.isRequired,
      startColor: PropTypes.string.isRequired,
    }),
    PropTypes.string,
  ]).isRequired,
  scale: PropTypes.shape({
    callback: PropTypes.func,
    duration: PropTypes.number,
    from: PropTypes.number,
    to: PropTypes.number,
  }),
  sides: PropTypes.number,
  size: PropTypes.number.isRequired,
};

Spinner.defaultProps = {
  className: null,
  duration: 2000,
  scale: null,
  sides: 6,
};

export default Spinner;
