/* TODO: Investigate why this is required. My instinct is that the storysource addon is causing the transpiler
output to break this rule, as commenting out its configuration in webpack config fixes the issue. */
/* eslint-disable import/first */
import React from "react";
import { storiesOf } from "@storybook/react";
import { action } from "@storybook/addon-actions";
import Spinner from "./Spinner";

storiesOf("Spinner", module)
  .add("a spinner!", () => <Spinner fill="#000" size={60} />)
  .add("a spinner but scales instantly!", () => (
    <Spinner
      fill="#000"
      scale={{
        from: 1,
        to: 100,
        duration: 1500,
        callback: action("Finished animating"),
      }}
      size={60}
    />
  ));
