import classnames from "classnames";
import React from "react";
import PropTypes from "prop-types";
import keycode from "keycode";
import Spinner from "../Spinner/Spinner";
import ModalButton from "./ModalButton";
import styles from "./Modal.module.css";

export default class Modal extends React.Component {
  backgroundRef = null;

  componentDidMount() {
    document.addEventListener("keydown", this.onKeydown);
  }

  componentWillUnmount() {
    document.removeEventListener("keydown", this.onKeydown);
  }

  onKeydown(e) {
    if (keycode.isEventKey(e, "esc")) {
      this.props.onCancel();
    }
  }

  onBackgroundClick = e => {
    // Check that the clicked element is the background. We don't want to fire onCancel
    // if the click event bubbled up from a child element.
    if (e.target === this.backgroundRef && this.props.clickingOutsideCancels) {
      this.props.onCancel();
    }
  };

  /* eslint-disable jsx-a11y/click-events-have-key-events,jsx-a11y/no-static-element-interactions,jsx-a11y/no-noninteractive-element-interactions,no-return-assign */
  render() {
    return (
      <div
        ref={ref => (this.backgroundRef = ref)}
        className={classnames(styles.background, {
          [styles.darkBackground]: this.props.darkenOutside,
        })}
        data-ref={this.props.dataRef}
        onClick={this.onBackgroundClick}
      >
        <div className={styles.container}>
          <Spinner
            className={this.props.className}
            duration={this.props.spinDuration}
            fill={this.props.fill}
            sides={this.props.sides}
            size={this.props.size}
          />
          <div className={styles.modalContent}>
            {this.props.children}
            <div className={styles.buttonContainer}>
              {this.props.buttons || (
                <>
                  <ModalButton
                    className={styles.button}
                    dataRef="confirmModalButton"
                    onClick={this.props.onConfirm}
                    type="button"
                  >
                    {this.props.okButtonText}
                  </ModalButton>
                  <ModalButton
                    className={`${styles.button} ${styles.cancelButton}`}
                    dataRef="cancelModalButton"
                    onClick={this.props.onCancel}
                    type="button"
                  >
                    {this.props.cancelButtonText}
                  </ModalButton>
                </>
              )}
            </div>
          </div>
        </div>
      </div>
    );
  }
  /* eslint-enable */
}

Modal.propTypes = {
  buttons: PropTypes.node,
  cancelButtonText: PropTypes.string,
  children: PropTypes.node,
  className: PropTypes.string,
  clickingOutsideCancels: PropTypes.bool,
  darkenOutside: PropTypes.bool,
  dataRef: PropTypes.string,
  fill: PropTypes.shape(),
  okButtonText: PropTypes.string,
  onCancel: PropTypes.func,
  onConfirm: PropTypes.func.isRequired,
  sides: PropTypes.number,
  size: PropTypes.number,
  spinDuration: PropTypes.number,
};

Modal.defaultProps = {
  buttons: null,
  cancelButtonText: "Cancel",
  children: null,
  className: null,
  clickingOutsideCancels: true,
  darkenOutside: true,
  dataRef: null,
  fill: {
    startColor: "#663CAB",
    endColor: "#EA5E48",
  },
  okButtonText: "OK",
  onCancel: null,
  sides: 10,
  size: 500,
  spinDuration: 80000,
};
