/* TODO: Investigate why this is required. My instinct is that the storysource addon is causing the transpiler
output to break this rule, as commenting out its configuration in webpack config fixes the issue. */
/* eslint-disable import/first */
import React from "react";
import { storiesOf } from "@storybook/react";
import { action } from "@storybook/addon-actions";
import Modal from "./Modal";

const whiteBackgroundDecorator = storyFn => (
  <div
    style={{
      position: "absolute",
      width: "100%",
      height: "100%",
      backgroundColor: "white",
    }}
  >
    {storyFn()}
  </div>
);

storiesOf("Modal", module)
  .addDecorator(whiteBackgroundDecorator)
  .add("default", () => (
    <Modal onCancel={action("Cancelled")} onConfirm={action("Confirmed")}>
      <p>This is a test message.</p>
    </Modal>
  ))
  .add("disabled outside click cancellation", () => (
    <Modal
      clickingOutsideCancels={false}
      onCancel={action("Cancelled")}
      onConfirm={action("Confirmed")}
    >
      <p>This is a test message.</p>
    </Modal>
  ))
  .add("disabled darkened outside", () => (
    <Modal
      darkenOutside={false}
      onCancel={action("Cancelled")}
      onConfirm={action("Confirmed")}
    >
      <p>This is a test message.</p>
    </Modal>
  ));
