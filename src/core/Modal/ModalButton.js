import React from "react";
import PropTypes from "prop-types";
import classnames from "classnames";
import styles from "./buttons.module.css";

const ModalButton = ({ children, className, dataRef, onClick }) => (
  <button
    className={classnames(styles.button, className)}
    data-ref={dataRef}
    onClick={onClick}
    type="button"
  >
    {children}
  </button>
);

ModalButton.propTypes = {
  children: PropTypes.node.isRequired,
  className: PropTypes.string,
  dataRef: PropTypes.string,
  onClick: PropTypes.func.isRequired,
};

ModalButton.defaultProps = {
  className: null,
  dataRef: null,
};

export default ModalButton;
