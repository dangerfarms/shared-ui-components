import React from "react";
import PropTypes from "prop-types";
import classnames from "classnames";
import styles from "./buttons.module.css";

const ModalSubmitButton = ({ children, className, dataRef, form }) => (
  <button
    className={classnames(styles.button, className)}
    data-ref={dataRef}
    form={form}
    type="submit"
  >
    {children}
  </button>
);

ModalSubmitButton.propTypes = {
  children: PropTypes.node.isRequired,
  className: PropTypes.string,
  dataRef: PropTypes.string,
  form: PropTypes.string.isRequired,
};

ModalSubmitButton.defaultProps = {
  className: null,
  dataRef: null,
};

export default ModalSubmitButton;
