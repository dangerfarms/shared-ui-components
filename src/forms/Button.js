import React from "react";
import PropTypes from "prop-types";
import classnames from "classnames";
import styles from "./Button.module.css";

/* eslint-disable react/button-has-type */
export default function Button({
  children,
  className,
  disabled,
  onClick,
  type,
}) {
  return (
    <button
      className={classnames(styles.defaultButton, className)}
      disabled={disabled}
      onClick={onClick}
      type={type}
    >
      {children}
    </button>
  );
}

Button.propTypes = {
  children: PropTypes.node.isRequired,
  className: PropTypes.string,
  disabled: PropTypes.bool,
  onClick: PropTypes.func.isRequired,
  type: PropTypes.string,
};

Button.defaultProps = {
  className: null,
  disabled: false,
  type: "button",
};
