/* TODO: Investigate why this is required. My instinct is that the storysource addon is causing the transpiler
output to break this rule, as commenting out its configuration in webpack config fixes the issue. */
/* eslint-disable import/first */
import React from "react";
import { storiesOf } from "@storybook/react";
import { action } from "@storybook/addon-actions";
import Button from "./Button";
import styles from "./ButtonStory.module.css";
import markdownNotes from "./buttonStoryNotes.md";

storiesOf("Button", module)
  .add(
    "a button!",
    () => (
      <Button className={styles.button} onClick={action("clicked")}>
        Hello Button
      </Button>
    ),
    { notes: { markdown: markdownNotes } },
  )
  .add("a few buttons!", () => (
    <React.Fragment>
      <Button className={styles.button1} onClick={action("clicked")}>
        Button 1
      </Button>
      <Button className={styles.button2} onClick={action("clicked")}>
        Button 2
      </Button>
      <Button className={styles.button3} onClick={action("clicked")}>
        Button 3
      </Button>
    </React.Fragment>
  ));
