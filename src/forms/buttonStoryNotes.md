### Example of Button component usage:

<br />

```react
<Button
  className={classnames(styles.voteYes, {
    [styles.vote__chosen]: userVote && userVote.accept,
  })}
  disabled={votingDisabled}
  onClick={() =>
    doVote({
      variables: {
        proposalId: props.proposalId,
        accept: true,
      },
    })
  }
>
```
