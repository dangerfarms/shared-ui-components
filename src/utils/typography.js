import Typography from "typography";

const baseFontColor = "rgba(255, 255, 255, 1)";

export const typography = new Typography({
  bodyColor: baseFontColor,
  bodyFontFamily: ["Lato", "Helvetica", "sans-serif"],
  googleFonts: [
    {
      name: "Lato",
      styles: ["400", "700", "900"],
    },
  ],
  headerColor: baseFontColor,
  headerFontFamily: ["titling-gothic-fb-condensed", "Helvetica", "sans-serif"],
});

typography.injectStyles();
