provider "google" {
  project = "danger-farms"
  region  = "europe-west1"
  zone    = "europe-west1"
}

terraform {
  backend "gcs" {
    bucket = "danger-farms-tf-state"
    prefix = "shared-ui-components"
  }
}

resource "google_storage_bucket" "shared_ui_components_storybook" {
  name     = "ui.dangerfarms.com"
  location = "EU"

  website {
    main_page_suffix = "index.html"
    not_found_page   = "404.html"
  }
}
