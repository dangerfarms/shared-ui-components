# Importing components

Components may be imported from this library as follows:

```
import { Polygon, Button } from "@dangerfarms/ui";

```

or, more selectively:

```
import { Polygon } from "@dangerfarms/ui/lib/core";
import { Button } from "@dangerfarms/ui/lib/forms";
```

(It does not appear to be possible, at present, to avoid including `lib/` in the selective import paths. PRs welcome if you know a way around this (other than publishing from `lib/` itself, which has unacceptable downsides).)

# Deploying Storybook

Storybook is rebuilt and redeployed on any push to master with changes to `stories.js` files.

# Publishing the npm package

In order to publish a new version of this package, both to ui.dangerfarms.com and to npm, simply run `npm version patch` (or any other npm-version-incrementing command).

This is the _only_ way in which releases should be made.

# Developing

In order to work on this package and see the results/effects when importing from it elsewhere, it is easiest to use `yarn link` (this avoids having to increment the version and re-publish in order to test each change).

With this repo checked out, run `yarn link`, then, in the repo importing it, run `yarn link "@dangerfarms/ui"`.

## Adding new components

If you add a new component that imports an external package, you will need to add the imported package to the appropriate `external`/`externals` array in `rollup.config.js`. For example, a component added to `core` which is also available to import from the root `index.js` file should have the package added to the `external` arrays within the corresponding config objects (i.e. those with `input: "src/core/index.js"` and `input: "src/index.js"`). A package that will be required by _all_ components can be added to the `externals` array near the top of the file.
