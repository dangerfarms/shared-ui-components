import resolve from "rollup-plugin-node-resolve";
import babel from "rollup-plugin-babel";
import postcss from "rollup-plugin-postcss";

const libraryDir = "lib";

const externals = ["classnames", "prop-types", "react", "typography"];

const plugins = [
  resolve({ only: `src/` }),
  babel({
    exclude: "node_modules/**",
    runtimeHelpers: true,
  }),
  postcss({
    modules: true,
  }),
];

export default [
  {
    input: "src/index.js",
    output: {
      file: `${libraryDir}/index.js`,
      format: "cjs",
    },
    external: [...externals, "animejs", "cuid", "keycode"],
    plugins,
  },
  {
    input: "src/core/index.js",
    output: {
      file: `${libraryDir}/core.js`,
      format: "cjs",
    },
    external: [...externals, "animejs", "cuid", "keycode"],
    plugins,
  },
  {
    input: "src/forms/index.js",
    output: [
      {
        file: `${libraryDir}/forms.js`,
        format: "cjs",
      },
    ],
    external: [...externals],
    plugins,
  },
];
