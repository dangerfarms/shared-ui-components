import { addParameters, configure } from "@storybook/react";
import dangerBankTheme from "./dangerBankTheme";
import "../src/utils/typography";

addParameters({
  options: {
    theme: dangerBankTheme,
  },
});

function loadStories() {
  const req = require.context("../src", true, /\.stories\.js$/);
  req.keys().forEach(filename => req(filename));
}

configure(loadStories, module);
